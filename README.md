# Notification Creation and Scheduling Dashboard
### Features ###
1. Schedule a notification with header, content, image, sending time for based on user properties.
2. View all notifications list as well as detailed information for each.
3. View all users list as well as detailed information for each.
4. Notification can only be created if image URL is valid (based on image mimetype) and header and content text meet min/max length requirements.
5. Notification are scheduled via celery and will be sent to devices of users matching user properties set in notification object.

### Set-up the project ###
**Pre-requisities:** mysql and rabbitmq should be running. 
For mysql there should be no password for root and for rabbitmq, the username and password should be guest and guest respectively.

1. pip install -r requirements.txt.
 (If you get the error "library not found for -lssl" while installing MySQL-python run the following:
xcode-select --install)
2. mysql -u root -e "CREATE DATABASE notifydb";
3. mysql -u root notifydb < notifydb.sql
4. python manage.py runserver
5. python manage.py celeryd_detach
6. Go to http://127.0.0.1:8000/dashboard/ to access the dashboard

### Additional possible feature extensions ###
1. Support personalized notifications - header and content can contain be personalized based on user properties.
2. For scalability, push ids can be queried with batches of user ids and notification can be sent to each batch of push ids.
3. Support unscheduling of notifications - Notifications scheduled but not sent, can be unscheduled.
4. Filter notifications based on status and sent time

### References ###

* https://docs.djangoproject.com/en/1.11/intro/tutorial01/
* https://stackoverflow.com/questions/14412211/get-mimetype-of-file-python
* http://www.django-rest-framework.org/#api-guide
* https://docs.djangoproject.com/en/1.11/ref/class-based-views/