-- MySQL dump 10.13  Distrib 5.7.18, for osx10.10 (x86_64)
--
-- Host: localhost    Database: notifydb
-- ------------------------------------------------------
-- Server version	5.6.36

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can add group',2,'add_group'),(5,'Can change group',2,'change_group'),(6,'Can delete group',2,'delete_group'),(7,'Can add permission',3,'add_permission'),(8,'Can change permission',3,'change_permission'),(9,'Can delete permission',3,'delete_permission'),(10,'Can add user',4,'add_user'),(11,'Can change user',4,'change_user'),(12,'Can delete user',4,'delete_user'),(13,'Can add content type',5,'add_contenttype'),(14,'Can change content type',5,'change_contenttype'),(15,'Can delete content type',5,'delete_contenttype'),(16,'Can add session',6,'add_session'),(17,'Can change session',6,'change_session'),(18,'Can delete session',6,'delete_session'),(19,'Can add question',7,'add_question'),(20,'Can change question',7,'change_question'),(21,'Can delete question',7,'delete_question'),(22,'Can add choice',8,'add_choice'),(23,'Can change choice',8,'change_choice'),(24,'Can delete choice',8,'delete_choice'),(25,'Can add notification data',9,'add_notificationdata'),(26,'Can change notification data',9,'change_notificationdata'),(27,'Can delete notification data',9,'delete_notificationdata'),(28,'Can add user',10,'add_user'),(29,'Can change user',10,'change_user'),(30,'Can delete user',10,'delete_user'),(31,'Can add device',11,'add_device'),(32,'Can change device',11,'change_device'),(33,'Can delete device',11,'delete_device'),(34,'Can add periodic task',12,'add_periodictask'),(35,'Can change periodic task',12,'change_periodictask'),(36,'Can delete periodic task',12,'delete_periodictask'),(37,'Can add crontab',13,'add_crontabschedule'),(38,'Can change crontab',13,'change_crontabschedule'),(39,'Can delete crontab',13,'delete_crontabschedule'),(40,'Can add interval',14,'add_intervalschedule'),(41,'Can change interval',14,'change_intervalschedule'),(42,'Can delete interval',14,'delete_intervalschedule'),(43,'Can add periodic tasks',15,'add_periodictasks'),(44,'Can change periodic tasks',15,'change_periodictasks'),(45,'Can delete periodic tasks',15,'delete_periodictasks'),(46,'Can add task state',16,'add_taskmeta'),(47,'Can change task state',16,'change_taskmeta'),(48,'Can delete task state',16,'delete_taskmeta'),(49,'Can add saved group result',17,'add_tasksetmeta'),(50,'Can change saved group result',17,'change_tasksetmeta'),(51,'Can delete saved group result',17,'delete_tasksetmeta'),(52,'Can add worker',18,'add_workerstate'),(53,'Can change worker',18,'change_workerstate'),(54,'Can delete worker',18,'delete_workerstate'),(55,'Can add task',19,'add_taskstate'),(56,'Can change task',19,'change_taskstate'),(57,'Can delete task',19,'delete_taskstate');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$36000$2qT6eFiTwLcJ$eMHRFZ3YAgs7BLMQuSBvnOJcegLoxWrHXjaOnUg/yO8=','2017-06-07 19:47:50.276705',1,'admin','','','admin@notifications.com',1,1,'2017-06-07 19:46:04.670902');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `celery_taskmeta`
--

DROP TABLE IF EXISTS `celery_taskmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `celery_taskmeta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` varchar(255) NOT NULL,
  `status` varchar(50) NOT NULL,
  `result` longtext,
  `date_done` datetime(6) NOT NULL,
  `traceback` longtext,
  `hidden` tinyint(1) NOT NULL,
  `meta` longtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `task_id` (`task_id`),
  KEY `celery_taskmeta_hidden_23fd02dc` (`hidden`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `celery_taskmeta`
--

LOCK TABLES `celery_taskmeta` WRITE;
/*!40000 ALTER TABLE `celery_taskmeta` DISABLE KEYS */;
INSERT INTO `celery_taskmeta` VALUES (1,'62cb0ce2-c4fd-495b-93af-418b62e8ea02','SUCCESS','gAKILg==','2017-06-11 12:03:11.823821',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(2,'6347ee3c-7c5f-46a7-aa0c-d4b51d938f7c','FAILURE','gAJjZGphbmdvLmNvcmUuZXhjZXB0aW9ucwpGaWVsZEVycm9yCnEBWH4AAABDYW5ub3QgcmVzb2x2ZSBrZXl3b3JkICdbdSd1c2VyX3Byb3BlcnR5J10nIGludG8gZmllbGQuIENob2ljZXMgYXJlOiBjcmVhdGVkX3RpbWUsIGRldmljZSwgaWQsIGluc3RhbGxlZCwgdXNlcl9jaXR5LCB1c2VyX25hbWVxAoVxA1JxBC4=','2017-06-11 17:54:07.701561','Traceback (most recent call last):\n  File \"/Library/Python/2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/Library/Python/2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/Users/shreyakedia/Documents/sample-app-dj/notification_dashboard/notifications/tasks.py\", line 10, in send_notification\n    user_ids = list(User.objects.filter(**kwargs).values_list(\'id\', flat=True))\n  File \"/Library/Python/2.7/site-packages/django/db/models/manager.py\", line 85, in manager_method\n    return getattr(self.get_queryset(), name)(*args, **kwargs)\n  File \"/Library/Python/2.7/site-packages/django/db/models/query.py\", line 784, in filter\n    return self._filter_or_exclude(False, *args, **kwargs)\n  File \"/Library/Python/2.7/site-packages/django/db/models/query.py\", line 802, in _filter_or_exclude\n    clone.query.add_q(Q(*args, **kwargs))\n  File \"/Library/Python/2.7/site-packages/django/db/models/sql/query.py\", line 1261, in add_q\n    clause, _ = self._add_q(q_object, self.used_aliases)\n  File \"/Library/Python/2.7/site-packages/django/db/models/sql/query.py\", line 1287, in _add_q\n    allow_joins=allow_joins, split_subq=split_subq,\n  File \"/Library/Python/2.7/site-packages/django/db/models/sql/query.py\", line 1165, in build_filter\n    lookups, parts, reffed_expression = self.solve_lookup_type(arg)\n  File \"/Library/Python/2.7/site-packages/django/db/models/sql/query.py\", line 1045, in solve_lookup_type\n    _, field, _, lookup_parts = self.names_to_path(lookup_splitted, self.get_meta())\n  File \"/Library/Python/2.7/site-packages/django/db/models/sql/query.py\", line 1363, in names_to_path\n    \"Choices are: %s\" % (name, \", \".join(available)))\nFieldError: Cannot resolve keyword \'[u\'user_property\']\' into field. Choices are: created_time, device, id, installed, user_city, user_name\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(3,'069e2052-5136-4112-b944-9ddd9e48f4b2','FAILURE','gAJjZGphbmdvLmNvcmUuZXhjZXB0aW9ucwpGaWVsZEVycm9yCnEBWH4AAABDYW5ub3QgcmVzb2x2ZSBrZXl3b3JkICdbdSd1c2VyX3Byb3BlcnR5J10nIGludG8gZmllbGQuIENob2ljZXMgYXJlOiBjcmVhdGVkX3RpbWUsIGRldmljZSwgaWQsIGluc3RhbGxlZCwgdXNlcl9jaXR5LCB1c2VyX25hbWVxAoVxA1JxBC4=','2017-06-11 17:57:09.985347','Traceback (most recent call last):\n  File \"/Library/Python/2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/Library/Python/2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/Users/shreyakedia/Documents/sample-app-dj/notification_dashboard/notifications/tasks.py\", line 10, in send_notification\n    print kwargs\n  File \"/Library/Python/2.7/site-packages/django/db/models/manager.py\", line 85, in manager_method\n    return getattr(self.get_queryset(), name)(*args, **kwargs)\n  File \"/Library/Python/2.7/site-packages/django/db/models/query.py\", line 784, in filter\n    return self._filter_or_exclude(False, *args, **kwargs)\n  File \"/Library/Python/2.7/site-packages/django/db/models/query.py\", line 802, in _filter_or_exclude\n    clone.query.add_q(Q(*args, **kwargs))\n  File \"/Library/Python/2.7/site-packages/django/db/models/sql/query.py\", line 1261, in add_q\n    clause, _ = self._add_q(q_object, self.used_aliases)\n  File \"/Library/Python/2.7/site-packages/django/db/models/sql/query.py\", line 1287, in _add_q\n    allow_joins=allow_joins, split_subq=split_subq,\n  File \"/Library/Python/2.7/site-packages/django/db/models/sql/query.py\", line 1165, in build_filter\n    lookups, parts, reffed_expression = self.solve_lookup_type(arg)\n  File \"/Library/Python/2.7/site-packages/django/db/models/sql/query.py\", line 1045, in solve_lookup_type\n    _, field, _, lookup_parts = self.names_to_path(lookup_splitted, self.get_meta())\n  File \"/Library/Python/2.7/site-packages/django/db/models/sql/query.py\", line 1363, in names_to_path\n    \"Choices are: %s\" % (name, \", \".join(available)))\nFieldError: Cannot resolve keyword \'[u\'user_property\']\' into field. Choices are: created_time, device, id, installed, user_city, user_name\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(4,'e871ba7e-e990-45f1-8e57-2cf3a5b9f1b7','FAILURE','gAJjZGphbmdvLmNvcmUuZXhjZXB0aW9ucwpGaWVsZEVycm9yCnEBWH4AAABDYW5ub3QgcmVzb2x2ZSBrZXl3b3JkICdbdSd1c2VyX3Byb3BlcnR5J10nIGludG8gZmllbGQuIENob2ljZXMgYXJlOiBjcmVhdGVkX3RpbWUsIGRldmljZSwgaWQsIGluc3RhbGxlZCwgdXNlcl9jaXR5LCB1c2VyX25hbWVxAoVxA1JxBC4=','2017-06-11 17:59:24.249657','Traceback (most recent call last):\n  File \"/Library/Python/2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/Library/Python/2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/Users/shreyakedia/Documents/sample-app-dj/notification_dashboard/notifications/tasks.py\", line 10, in send_notification\n    print kwargs\n  File \"/Library/Python/2.7/site-packages/django/db/models/manager.py\", line 85, in manager_method\n    return getattr(self.get_queryset(), name)(*args, **kwargs)\n  File \"/Library/Python/2.7/site-packages/django/db/models/query.py\", line 784, in filter\n    return self._filter_or_exclude(False, *args, **kwargs)\n  File \"/Library/Python/2.7/site-packages/django/db/models/query.py\", line 802, in _filter_or_exclude\n    clone.query.add_q(Q(*args, **kwargs))\n  File \"/Library/Python/2.7/site-packages/django/db/models/sql/query.py\", line 1261, in add_q\n    clause, _ = self._add_q(q_object, self.used_aliases)\n  File \"/Library/Python/2.7/site-packages/django/db/models/sql/query.py\", line 1287, in _add_q\n    allow_joins=allow_joins, split_subq=split_subq,\n  File \"/Library/Python/2.7/site-packages/django/db/models/sql/query.py\", line 1165, in build_filter\n    lookups, parts, reffed_expression = self.solve_lookup_type(arg)\n  File \"/Library/Python/2.7/site-packages/django/db/models/sql/query.py\", line 1045, in solve_lookup_type\n    _, field, _, lookup_parts = self.names_to_path(lookup_splitted, self.get_meta())\n  File \"/Library/Python/2.7/site-packages/django/db/models/sql/query.py\", line 1363, in names_to_path\n    \"Choices are: %s\" % (name, \", \".join(available)))\nFieldError: Cannot resolve keyword \'[u\'user_property\']\' into field. Choices are: created_time, device, id, installed, user_city, user_name\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(5,'c9a5cba7-68d4-4172-bbbc-9e16df245a81','FAILURE','gAJjZGphbmdvLmNvcmUuZXhjZXB0aW9ucwpGaWVsZEVycm9yCnEBWH4AAABDYW5ub3QgcmVzb2x2ZSBrZXl3b3JkICdbdSd1c2VyX3Byb3BlcnR5J10nIGludG8gZmllbGQuIENob2ljZXMgYXJlOiBjcmVhdGVkX3RpbWUsIGRldmljZSwgaWQsIGluc3RhbGxlZCwgdXNlcl9jaXR5LCB1c2VyX25hbWVxAoVxA1JxBC4=','2017-06-11 18:00:30.371668','Traceback (most recent call last):\n  File \"/Library/Python/2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/Library/Python/2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\n  File \"/Users/shreyakedia/Documents/sample-app-dj/notification_dashboard/notifications/tasks.py\", line 10, in send_notification\n    print kwargs\n  File \"/Library/Python/2.7/site-packages/django/db/models/manager.py\", line 85, in manager_method\n    return getattr(self.get_queryset(), name)(*args, **kwargs)\n  File \"/Library/Python/2.7/site-packages/django/db/models/query.py\", line 784, in filter\n    return self._filter_or_exclude(False, *args, **kwargs)\n  File \"/Library/Python/2.7/site-packages/django/db/models/query.py\", line 802, in _filter_or_exclude\n    clone.query.add_q(Q(*args, **kwargs))\n  File \"/Library/Python/2.7/site-packages/django/db/models/sql/query.py\", line 1261, in add_q\n    clause, _ = self._add_q(q_object, self.used_aliases)\n  File \"/Library/Python/2.7/site-packages/django/db/models/sql/query.py\", line 1287, in _add_q\n    allow_joins=allow_joins, split_subq=split_subq,\n  File \"/Library/Python/2.7/site-packages/django/db/models/sql/query.py\", line 1165, in build_filter\n    lookups, parts, reffed_expression = self.solve_lookup_type(arg)\n  File \"/Library/Python/2.7/site-packages/django/db/models/sql/query.py\", line 1045, in solve_lookup_type\n    _, field, _, lookup_parts = self.names_to_path(lookup_splitted, self.get_meta())\n  File \"/Library/Python/2.7/site-packages/django/db/models/sql/query.py\", line 1363, in names_to_path\n    \"Choices are: %s\" % (name, \", \".join(available)))\nFieldError: Cannot resolve keyword \'[u\'user_property\']\' into field. Choices are: created_time, device, id, installed, user_city, user_name\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(6,'018ef24a-b685-4dee-9059-7278a1b22f81','SUCCESS',NULL,'2017-06-11 18:02:04.589587',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZA5VSUxKS0syTkrUNUkzStE1MTNI0U0yMkrUNU82SDG2TDZJTTQwLGTxa/NrK2RNLNYDAOYeFLU='),(7,'abffb3ba-4f2d-460d-b22a-7c0d39c4ea01','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVN3NlbmRfbm90aWZpY2F0aW9uKCkgdGFrZXMgZXhhY3RseSAzIGFyZ3VtZW50cyAoMiBnaXZlbilxAoVxA1JxBC4=','2017-06-11 18:02:04.612678','Traceback (most recent call last):\n  File \"/Library/Python/2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/Library/Python/2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\nTypeError: send_notification() takes exactly 3 arguments (2 given)\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(8,'38cf3d70-b8fc-4176-9d28-b42a8662de95','SUCCESS',NULL,'2017-06-11 18:02:54.660646',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZA5VSbG0NDZONDDQNTRPtNA1STNP0k00MUjWNTc2SDQxMTBKMTBMKmTxa/NrK2RNLNYDALYjE2Y='),(9,'d9933a00-17a8-4f7b-a40c-730a4402d01b','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVN3NlbmRfbm90aWZpY2F0aW9uKCkgdGFrZXMgZXhhY3RseSAzIGFyZ3VtZW50cyAoMiBnaXZlbilxAoVxA1JxBC4=','2017-06-11 18:02:54.684471','Traceback (most recent call last):\n  File \"/Library/Python/2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/Library/Python/2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\nTypeError: send_notification() takes exactly 3 arguments (2 given)\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(10,'0ebeb1bc-9977-4bae-8e2a-7021d16ba277','SUCCESS',NULL,'2017-06-11 18:04:18.038853',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZA5VsUxONkhOMTfXNTQ1TtY1SUu20E1MskzTTUo2NUxNTUkzNbA0LWTxa/NrK2RNLNYDANr9FKo='),(11,'9cc0cd77-153c-4fc8-ab9f-bc51eedf5095','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVN3NlbmRfbm90aWZpY2F0aW9uKCkgdGFrZXMgZXhhY3RseSAzIGFyZ3VtZW50cyAoMiBnaXZlbilxAoVxA1JxBC4=','2017-06-11 18:04:18.056953','Traceback (most recent call last):\n  File \"/Library/Python/2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/Library/Python/2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\nTypeError: send_notification() takes exactly 3 arguments (2 given)\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(12,'c3d2a760-e811-4166-8729-c45ddeab1e67','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVN3NlbmRfbm90aWZpY2F0aW9uKCkgdGFrZXMgZXhhY3RseSAzIGFyZ3VtZW50cyAoNCBnaXZlbilxAoVxA1JxBC4=','2017-06-11 18:17:27.630278','Traceback (most recent call last):\n  File \"/Library/Python/2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/Library/Python/2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\nTypeError: send_notification() takes exactly 3 arguments (4 given)\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(13,'0221ddaf-d2a0-4ea8-ad36-844275e32288','FAILURE','gAJjZXhjZXB0aW9ucwpUeXBlRXJyb3IKcQFVN3NlbmRfbm90aWZpY2F0aW9uKCkgdGFrZXMgZXhhY3RseSAzIGFyZ3VtZW50cyAoNCBnaXZlbilxAoVxA1JxBC4=','2017-06-11 18:20:13.530685','Traceback (most recent call last):\n  File \"/Library/Python/2.7/site-packages/celery/app/trace.py\", line 240, in trace_task\n    R = retval = fun(*args, **kwargs)\n  File \"/Library/Python/2.7/site-packages/celery/app/trace.py\", line 438, in __protected_call__\n    return self.run(*args, **kwargs)\nTypeError: send_notification() takes exactly 3 arguments (4 given)\n',0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(14,'6a995be3-354a-4770-a2ab-04b8466bea3e','SUCCESS',NULL,'2017-06-11 18:20:52.159700',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZA5VMTNPSzFLSknVtTAwMNA1MTRI0bU0NTDTNTdMtkhLA3JNk40LWfza/NoKWROL9QC/qxOt'),(15,'67fd6bde-8000-410d-9506-71c8ff10d5c3','SUCCESS','gAKILg==','2017-06-11 18:20:52.212044',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(16,'a6f6be67-5217-4a6e-965a-de715c1277f0','SUCCESS',NULL,'2017-06-11 18:27:29.027917',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZA5VSTY2NbJMNUjRTUsyS9Q1STUz0bVISkvVNU1JTbVMNktNTElKKmTxa/NrK2RNLNYDAODzFTo='),(17,'c3529e0d-fb6a-4e64-8bfe-5dee9c6eadbb','SUCCESS','gAKILg==','2017-06-11 18:27:29.086923',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(18,'26b60bcd-77c7-43a5-a207-609d4ff5a005','SUCCESS',NULL,'2017-06-11 18:30:19.235155',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZA5VMUo0TDQ2sjDXNTIyMdY1SUtN0k0ySk7TTTYxMjVINktKNkhLK2Txa/NrK2RNLNYDAMKoFCs='),(19,'2a1a3287-2243-4feb-b2cf-c4250c6bc0ff','SUCCESS','gAKILg==','2017-06-11 18:30:19.270274',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(20,'6b77efbc-42d9-4811-a769-e5e549c71ae3','SUCCESS',NULL,'2017-06-11 18:43:44.891982',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZA5VsUxOSbJINLDUTbO0TNU1MTZO07U0SjXSNbawNLVMNDU1NUtKK2Txa/NrK2RNLNYDAMenE9Q='),(21,'9cdb8a09-f99e-433f-92e2-38959a5556bf','SUCCESS','gAKILg==','2017-06-11 18:43:44.975982',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(22,'69384356-cb85-43a2-af22-3874bd81925d','SUCCESS',NULL,'2017-06-11 18:46:15.138999',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZA5VsUgzSjNJTrHUtbAwN9M1STYz1rVMtTDQNUpKNLI0T0s0tLBMLmTxa/NrK2RNLNYDAMJ7E9E='),(23,'8f2f4cd9-8876-4c63-9e80-2ba297fa189c','SUCCESS','gAKILg==','2017-06-11 18:46:15.225827',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(24,'5e07f0cb-6d18-4d40-b4e6-e580f6374392','SUCCESS',NULL,'2017-06-11 19:46:18.201275',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZA5VSTQ3SE61SEnRNUhKM9Q1MTMy0k0ytUjSTbVMSU4ySTZMTTEyKmTxa/NrK2RNLNYDANkLFJU='),(25,'a70ce8dd-0bf1-4622-b58b-e9dcb4c1ed22','SUCCESS','gAKILg==','2017-06-11 19:46:18.295267',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA'),(26,'28add8a4-9aa0-40dc-9238-ef3bc26b0ff4','SUCCESS',NULL,'2017-06-11 20:22:13.347216',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZA5VMbA0NTMyM0zSNTQ0S9I1SUw21rUwMjXRTTY0STI2tTRItTA3LGTxa/NrK2RNLNYDAKWPEug='),(27,'0956261b-116b-4ac3-8254-c14b3590e871','SUCCESS','gAKILg==','2017-06-11 20:22:13.635352',NULL,0,'eJxrYKotZIzgYGBgSM7IzEkpSs0rZIotZC7WAwBWuwcA');
/*!40000 ALTER TABLE `celery_taskmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `celery_tasksetmeta`
--

DROP TABLE IF EXISTS `celery_tasksetmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `celery_tasksetmeta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taskset_id` varchar(255) NOT NULL,
  `result` longtext NOT NULL,
  `date_done` datetime(6) NOT NULL,
  `hidden` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `taskset_id` (`taskset_id`),
  KEY `celery_tasksetmeta_hidden_593cfc24` (`hidden`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `celery_tasksetmeta`
--

LOCK TABLES `celery_tasksetmeta` WRITE;
/*!40000 ALTER TABLE `celery_tasksetmeta` DISABLE KEYS */;
/*!40000 ALTER TABLE `celery_tasksetmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'admin','logentry'),(2,'auth','group'),(3,'auth','permission'),(4,'auth','user'),(5,'contenttypes','contenttype'),(13,'djcelery','crontabschedule'),(14,'djcelery','intervalschedule'),(12,'djcelery','periodictask'),(15,'djcelery','periodictasks'),(16,'djcelery','taskmeta'),(17,'djcelery','tasksetmeta'),(19,'djcelery','taskstate'),(18,'djcelery','workerstate'),(8,'notifications','choice'),(11,'notifications','device'),(9,'notifications','notificationdata'),(7,'notifications','question'),(10,'notifications','user'),(6,'sessions','session');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2017-06-07 19:22:30.265751'),(2,'auth','0001_initial','2017-06-07 19:22:31.009542'),(3,'admin','0001_initial','2017-06-07 19:22:31.167567'),(4,'admin','0002_logentry_remove_auto_add','2017-06-07 19:22:31.225235'),(5,'contenttypes','0002_remove_content_type_name','2017-06-07 19:22:31.333065'),(6,'auth','0002_alter_permission_name_max_length','2017-06-07 19:22:31.391378'),(7,'auth','0003_alter_user_email_max_length','2017-06-07 19:22:31.453255'),(8,'auth','0004_alter_user_username_opts','2017-06-07 19:22:31.474415'),(9,'auth','0005_alter_user_last_login_null','2017-06-07 19:22:31.526895'),(10,'auth','0006_require_contenttypes_0002','2017-06-07 19:22:31.531646'),(11,'auth','0007_alter_validators_add_error_messages','2017-06-07 19:22:31.549890'),(12,'auth','0008_alter_user_username_max_length','2017-06-07 19:22:31.604992'),(13,'sessions','0001_initial','2017-06-07 19:22:31.669194'),(14,'notifications','0001_initial','2017-06-07 19:36:40.852732'),(15,'notifications','0002_auto_20170610_1214','2017-06-10 12:14:48.700489'),(16,'notifications','0003_remove_notificationdata_votes','2017-06-10 12:56:37.763563'),(17,'djcelery','0001_initial','2017-06-10 16:46:40.825155'),(18,'notifications','0004_auto_20170610_1929','2017-06-10 19:29:23.855022'),(19,'notifications','0005_auto_20170611_1813','2017-06-11 18:14:05.029026'),(20,'notifications','0003_auto_20170610_1929','2017-06-11 19:45:48.476353'),(21,'notifications','0004_auto_20170611_1813','2017-06-11 19:45:48.591066');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('peym1fnmgecfcwfdytwr3erhjgkmec7x','NDBiN2M0MDkyM2IwN2YwOWI1NDFmNWI1MGM3ODU4YjA1NzJmYjU4Mzp7Il9hdXRoX3VzZXJfaGFzaCI6IjFkMDM1NDI2ZmVmZTFjNDczZGNkZWI4ZTEzZjg5NTQ3MGMwYTNlNTQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2017-06-21 19:47:50.287520');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `djcelery_crontabschedule`
--

DROP TABLE IF EXISTS `djcelery_crontabschedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `djcelery_crontabschedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `minute` varchar(64) NOT NULL,
  `hour` varchar(64) NOT NULL,
  `day_of_week` varchar(64) NOT NULL,
  `day_of_month` varchar(64) NOT NULL,
  `month_of_year` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `djcelery_crontabschedule`
--

LOCK TABLES `djcelery_crontabschedule` WRITE;
/*!40000 ALTER TABLE `djcelery_crontabschedule` DISABLE KEYS */;
/*!40000 ALTER TABLE `djcelery_crontabschedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `djcelery_intervalschedule`
--

DROP TABLE IF EXISTS `djcelery_intervalschedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `djcelery_intervalschedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `every` int(11) NOT NULL,
  `period` varchar(24) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `djcelery_intervalschedule`
--

LOCK TABLES `djcelery_intervalschedule` WRITE;
/*!40000 ALTER TABLE `djcelery_intervalschedule` DISABLE KEYS */;
/*!40000 ALTER TABLE `djcelery_intervalschedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `djcelery_periodictask`
--

DROP TABLE IF EXISTS `djcelery_periodictask`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `djcelery_periodictask` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `task` varchar(200) NOT NULL,
  `args` longtext NOT NULL,
  `kwargs` longtext NOT NULL,
  `queue` varchar(200) DEFAULT NULL,
  `exchange` varchar(200) DEFAULT NULL,
  `routing_key` varchar(200) DEFAULT NULL,
  `expires` datetime(6) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL,
  `last_run_at` datetime(6) DEFAULT NULL,
  `total_run_count` int(10) unsigned NOT NULL,
  `date_changed` datetime(6) NOT NULL,
  `description` longtext NOT NULL,
  `crontab_id` int(11) DEFAULT NULL,
  `interval_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `djcelery_periodictas_crontab_id_75609bab_fk_djcelery_` (`crontab_id`),
  KEY `djcelery_periodictas_interval_id_b426ab02_fk_djcelery_` (`interval_id`),
  CONSTRAINT `djcelery_periodictas_crontab_id_75609bab_fk_djcelery_` FOREIGN KEY (`crontab_id`) REFERENCES `djcelery_crontabschedule` (`id`),
  CONSTRAINT `djcelery_periodictas_interval_id_b426ab02_fk_djcelery_` FOREIGN KEY (`interval_id`) REFERENCES `djcelery_intervalschedule` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `djcelery_periodictask`
--

LOCK TABLES `djcelery_periodictask` WRITE;
/*!40000 ALTER TABLE `djcelery_periodictask` DISABLE KEYS */;
/*!40000 ALTER TABLE `djcelery_periodictask` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `djcelery_periodictasks`
--

DROP TABLE IF EXISTS `djcelery_periodictasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `djcelery_periodictasks` (
  `ident` smallint(6) NOT NULL,
  `last_update` datetime(6) NOT NULL,
  PRIMARY KEY (`ident`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `djcelery_periodictasks`
--

LOCK TABLES `djcelery_periodictasks` WRITE;
/*!40000 ALTER TABLE `djcelery_periodictasks` DISABLE KEYS */;
/*!40000 ALTER TABLE `djcelery_periodictasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `djcelery_taskstate`
--

DROP TABLE IF EXISTS `djcelery_taskstate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `djcelery_taskstate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` varchar(64) NOT NULL,
  `task_id` varchar(36) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `tstamp` datetime(6) NOT NULL,
  `args` longtext,
  `kwargs` longtext,
  `eta` datetime(6) DEFAULT NULL,
  `expires` datetime(6) DEFAULT NULL,
  `result` longtext,
  `traceback` longtext,
  `runtime` double DEFAULT NULL,
  `retries` int(11) NOT NULL,
  `hidden` tinyint(1) NOT NULL,
  `worker_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `task_id` (`task_id`),
  KEY `djcelery_taskstate_state_53543be4` (`state`),
  KEY `djcelery_taskstate_name_8af9eded` (`name`),
  KEY `djcelery_taskstate_tstamp_4c3f93a1` (`tstamp`),
  KEY `djcelery_taskstate_hidden_c3905e57` (`hidden`),
  KEY `djcelery_taskstate_worker_id_f7f57a05_fk_djcelery_workerstate_id` (`worker_id`),
  CONSTRAINT `djcelery_taskstate_worker_id_f7f57a05_fk_djcelery_workerstate_id` FOREIGN KEY (`worker_id`) REFERENCES `djcelery_workerstate` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `djcelery_taskstate`
--

LOCK TABLES `djcelery_taskstate` WRITE;
/*!40000 ALTER TABLE `djcelery_taskstate` DISABLE KEYS */;
/*!40000 ALTER TABLE `djcelery_taskstate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `djcelery_workerstate`
--

DROP TABLE IF EXISTS `djcelery_workerstate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `djcelery_workerstate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hostname` varchar(255) NOT NULL,
  `last_heartbeat` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `hostname` (`hostname`),
  KEY `djcelery_workerstate_last_heartbeat_4539b544` (`last_heartbeat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `djcelery_workerstate`
--

LOCK TABLES `djcelery_workerstate` WRITE;
/*!40000 ALTER TABLE `djcelery_workerstate` DISABLE KEYS */;
/*!40000 ALTER TABLE `djcelery_workerstate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifications_device`
--

DROP TABLE IF EXISTS `notifications_device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifications_device` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_time` datetime(6) NOT NULL,
  `push_id` varchar(200) NOT NULL,
  `os` varchar(20) NOT NULL,
  `installed` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `notifications_device_user_id_57172e7e_fk_notifications_user_id` (`user_id`),
  CONSTRAINT `notifications_device_user_id_57172e7e_fk_notifications_user_id` FOREIGN KEY (`user_id`) REFERENCES `notifications_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifications_device`
--

LOCK TABLES `notifications_device` WRITE;
/*!40000 ALTER TABLE `notifications_device` DISABLE KEYS */;
INSERT INTO `notifications_device` VALUES (1,'2017-06-11 19:10:53.428747','push_id_1','iOS',1,3),(2,'2017-06-11 19:12:12.636454','push_id_2','Android',1,5),(3,'2017-06-11 19:12:30.301646','push_id_3','Android',1,6);
/*!40000 ALTER TABLE `notifications_device` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifications_notificationdata`
--

DROP TABLE IF EXISTS `notifications_notificationdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifications_notificationdata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `notification_name` varchar(50) NOT NULL,
  `header` varchar(150) NOT NULL,
  `content` varchar(300) NOT NULL,
  `image_url` varchar(200) NOT NULL,
  `created_time` datetime(6) NOT NULL,
  `send_time` datetime(6) NOT NULL,
  `updated_time` datetime(6) NOT NULL,
  `user_property` varchar(50) NOT NULL,
  `user_property_values` varchar(1000) NOT NULL,
  `sent_status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifications_notificationdata`
--

LOCK TABLES `notifications_notificationdata` WRITE;
/*!40000 ALTER TABLE `notifications_notificationdata` DISABLE KEYS */;
INSERT INTO `notifications_notificationdata` VALUES (1,'Test Notification 1','Hello! This is a test notification!','Hey there! Special offer for you!','https://i.imgur.com/2aK1hVm.jpg','2017-06-11 19:46:16.117286','2017-06-11 12:22:11.000000','2017-06-11 19:46:16.117297','id','1\r\n3\r\n6',1),(2,'Test Notification 2','Hello! This is another test notification!','Here are some product recommendations for you!','https://i.imgur.com/2aK1hVm.jpg','2017-06-11 19:49:35.935126','2017-06-11 20:22:11.000000','2017-06-11 19:49:35.935250','user_name','John\r\nAlice',1),(3,'Test Notification 3','Hello! This is yet another test notification!','Here are some product recommendations for you!','https://i.imgur.com/2aK1hVm.jpg','2017-06-11 19:50:54.739323','2017-06-12 01:22:11.000000','2017-06-11 19:50:54.739332','user_name','John\r\nAlice',0),(4,'Test Notification 4','One more test notification!','Here are some product recommendations for you!','https://i.imgur.com/2aK1hVm.jpg','2017-06-11 19:56:08.060407','2017-06-12 01:22:11.000000','2017-06-11 19:56:08.060416','user_name','John\r\nAlice',0),(5,'Test Notification 5','Yet one more test notification!','Here are some product recommendations for you!','https://i.imgur.com/2aK1hVm.jpg','2017-06-11 19:57:02.024916','2017-06-12 01:27:11.000000','2017-06-11 19:57:02.024934','user_name','John\r\nAlice',0);
/*!40000 ALTER TABLE `notifications_notificationdata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifications_user`
--

DROP TABLE IF EXISTS `notifications_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifications_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(200) DEFAULT NULL,
  `created_time` datetime(6) NOT NULL,
  `installed` tinyint(1) NOT NULL,
  `user_city` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_name_index` (`user_name`),
  KEY `user_city_index` (`user_city`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifications_user`
--

LOCK TABLES `notifications_user` WRITE;
/*!40000 ALTER TABLE `notifications_user` DISABLE KEYS */;
INSERT INTO `notifications_user` VALUES (1,'Shreya','2017-06-10 12:52:45.348675',1,'Bangalore'),(2,'Test User','2017-06-10 19:30:29.391632',1,'Kolkata'),(3,'John','2017-06-11 19:05:14.801389',1,'Mumbai'),(4,'Jane','2017-06-11 19:05:29.201684',1,'Mumbai'),(5,'Alice','2017-06-11 19:05:46.717265',1,'Delhi'),(6,'Bob','2017-06-11 19:06:08.481152',1,'Chennai');
/*!40000 ALTER TABLE `notifications_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-06-12  2:35:58
