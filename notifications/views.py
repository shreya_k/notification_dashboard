import datetime

from django.core.exceptions import ValidationError
from django.views import generic
from django.http import HttpResponse
from rest_framework import status
from rest_framework.response import Response
from django.shortcuts import render
from django.views.generic.edit import CreateView
from rest_framework.renderers import TemplateHTMLRenderer

from .models import User, NotificationData
from .serializers import UserSerializer, NotificationDataSerializer
from .tasks import send_notification


def index(request):
    """ Renders index page """
    template_name = 'notifications/index.html'
    return render(request, template_name)


class AllNotificationsView(generic.ListView):
    """ View for all notifications data"""
    template_name = 'notifications/all_notifications.html'
    context_object_name = 'notifications_list'

    def get_queryset(self):
        """ Get all notifications in reverse order of created time"""
        return NotificationData.objects.order_by('-created_time')


class NotificationDetails(generic.DetailView):
    """View for notification details"""
    model = NotificationData
    template_name = 'notifications/notification_detail.html'
    context_object_name = 'notification_data'


class AllUsersView(generic.ListView):
    """ View for all users' data """
    template_name = 'notifications/all_users.html'
    context_object_name = 'users_list'

    def get_queryset(self):
        return User.objects.all()


class UserDetails(generic.DetailView):
    """View for user details"""
    model = User
    template_name = 'notifications/user_detail.html'
    context_object_name = 'user'


class NotificationCreateView(CreateView):
    """ View to schedule a notification"""
    model = NotificationData
    # form_class = 'notifications/notificationdata_form.html'
    fields = ['header', 'content', 'image_url', 'send_time', 'user_property', 'user_property_values',
              'notification_name']
    # renderer_classes = [TemplateHTMLRenderer]

    def post(self, request):
        request_data = request._post
        serializer = NotificationDataSerializer(data=request_data)
        # If data is serialized successfully, return a successful response, else return errors
        try:
            if serializer.is_valid():
                notification_obj = serializer.save()
                send_time = datetime.datetime.strptime(request_data['send_time'], "%Y-%m-%d %H:%M:%S")
                user_property = request_data['user_property']
                user_property_values = request_data['user_property_values']
                payload = {
                    'header': request_data['header'],
                    'content': request_data['content'],
                    'image_url': request_data['image_url']
                }
                send_notification.apply_async(
                    (user_property, user_property_values, payload, notification_obj.id), eta=send_time)
                return HttpResponse("Successfully scheduled notification")
            else:
                return HttpResponse("Errors: " + str(serializer.errors))
        except ValidationError, e:
            return HttpResponse("Error in validating notification data: " + str(e))
