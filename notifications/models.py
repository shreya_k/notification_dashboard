# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import mimetypes

from django.core.exceptions import ValidationError
from django.core.validators import MinLengthValidator
from django.db import models
from django.utils import timezone
from django_mysql.models import Model


class User(Model):
    """ User model - contains user attributes"""
    user_name = models.CharField(max_length=200, blank=True, null=True)
    created_time = models.DateTimeField()
    user_city = models.CharField(max_length=200, blank=True, null=True)
    # To track whether user has uninstalled from all devices
    installed = models.BooleanField(default=True)

    def __str__(self):
        return self.user_name


class Device(Model):
    """ Device model - contains device attributes"""
    # Each user can have multiple devices, so map device to user
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created_time = models.DateTimeField()
    # Device Push ID, such as Registration ID in Android, Push token in iOS
    push_id = models.CharField(max_length=200)
    # OS platform - Android, iOS etc.
    os = models.CharField(max_length=20)
    # To track whether device has been uninstalled so notification should not be attempted next time
    installed = models.BooleanField(default=True)


def validate_image_url(url):
    """ Validate mime type from image url """
    mime_type, enc = mimetypes.guess_type(url)
    return mime_type and mime_type.startswith('image')


def validate_send_time(send_time):
    """ Validate if send time is greater than current time """
    return send_time > timezone.now()


class NotificationData(Model):
    """ Notification Data model - will store payload, schedule, status, and user properties to which notification
    should be sent """
    notification_name = models.CharField(max_length=50)
    header = models.CharField(max_length=150, validators=[MinLengthValidator(20)])
    content = models.CharField(max_length=300, validators=[MinLengthValidator(20)])
    image_url = models.URLField(validators=[validate_image_url])
    created_time = models.DateTimeField(editable=False)
    send_time = models.DateTimeField(validators=[validate_send_time])
    updated_time = models.DateTimeField()

    # User property using which users can be chosen - for e.g., id, user_name, user_city
    user_property = models.CharField(max_length=50)
    # Values for the selected user property
    user_property_values = models.CharField(max_length=1000)
    # To track whether notification has been sent
    sent_status = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        if not validate_image_url(self.image_url):
            raise ValidationError('Not a valid image url: MIME type is not image')
        if not validate_send_time(self.send_time):
            raise ValidationError('Scheduled time should be greater than current time')
        if not self.id:
            self.created_time = timezone.now()
        self.updated_time = timezone.now()
        return super(NotificationData, self).save(*args, **kwargs)
