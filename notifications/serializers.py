from rest_framework import serializers
from .models import NotificationData, User


class NotificationDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = NotificationData
        fields = ('header', 'content', 'image_url', 'created_time', 'updated_time', 'send_time', 'user_property',
                  'user_property_values', 'notification_name')
        read_only_fields = ('created_time', 'updated_time')


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'user_name', 'created_time', 'installed', 'user_city')
