from django.conf.urls import url

from . import views

app_name = 'notifications'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^notifications/all/$', views.AllNotificationsView.as_view(), name='allnotifications'),
    url(r'^users/$', views.AllUsersView.as_view(), name='users'),
    url(r'^users/(?P<pk>[0-9]+)/$', views.UserDetails.as_view(), name='user_detail'),
    url(r'^notifications/create/$', views.NotificationCreateView.as_view(), name='create'),
    url(r'^notifications/(?P<pk>[0-9]+)/$', views.NotificationDetails.as_view(), name='notification_detail'),
]
