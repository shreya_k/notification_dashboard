from .celery import app as celery_app
from .models import User, Device, NotificationData


@celery_app.task(name='send_notification')
def send_notification(user_property, user_property_values, notification_payload, notification_id):
    """ Get users as per user property values and send notification"""
    kwargs = {
        '{0}__{1}'.format(user_property, 'in'): user_property_values.split('\n')
    }
    # Get user ids filtered from user property values
    user_ids = list(User.objects.filter(**kwargs).values_list('id', flat=True))
    send_notification_to_user_ids.apply_async(args=(user_ids, notification_payload, notification_id))


@celery_app.task(name='send_notification_to_user_ids')
def send_notification_to_user_ids(user_ids, notification_payload, notification_id):
    """ Get push ids corresponding to users' devices and send notification """
    push_ids = list(Device.objects.filter(user__in=user_ids).values_list('push_id', flat=True))

    # Send notification to push ids

    # Update notification status to sent
    NotificationData.objects.filter(pk=notification_id).update(sent_status=True)
    return True
