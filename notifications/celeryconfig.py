BROKER_URL = 'amqp://guest:guest@127.0.01:5672'
CELERY_ACCEPT_CONTENT = ['json']

CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'

CELERY_ENABLE_UTC = False
